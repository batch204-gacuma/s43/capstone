const Product = require("../models/Product");
const User = require("../models/User");

module.exports.addProduct = (reqBody) => {

	let newProduct = new Product({

		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});


	// Saves the created object to our database
	return newProduct.save().then( (product, error) => {

		// Course creation successful
		if(error) {

			return false
		
		// Course creation failed
		} else {

			return true
		}


	})
}



//Controller for retrieving all ACTIVE courses
module.exports.getAllActive = () => {

	return Product.find({isActive: true}).then( result => {
		return result;
	})

}

//Retrieving a specific product
module.exports.getProduct = (reqParams) => {


    return Product.findById(reqParams.productId).then(result => {
        
        return result
    
    })

}

//Updating Product
module.exports.updateProduct = (reqParams, reqBody, data) => {

	return User.findById(data.id).then(result => {
		console.log(result)

		if(result.isAdmin === true) {

			// Specify the fields/properties of the document to be updated
			let updatedProduct = {
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			};

		
			return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {

			
				if(error) {

					return false

				// Course updated successfully	
				} else {

					return true
				}
			})

		} else {

			return false
		}

	})


}



//Updating Product

// module.exports.archiveProduct = (data, reqBody) => {

// 	return Product.findById(data.productId).then(result => {

// 		if (data.payload === true) {

// 			let updateActiveField = {
// 				isActive: reqBody.isActive
// 			}

// 			return Product.findByIdAndUpdate(result._id, updateActiveField).then((product, err) => {

// 					if(err) {
					
// 						return false
					
// 					}  else {

// 						return 
// 					}
// 			})
			 
// 		} else {

// 			return false
// 		} 
// 	})

// }

module.exports.archiveProduct = (data) => {

	return Product.findById(data.productId).then((result, err) => {

		if(data.payload === true) {

			result.isActive = false;

			return result.save().then((archivedProduct, err) => {

				// Product not archived
				if(err) {

					return false;

				// Product archived successfully
				} else {

					return true;
				}
			})

		} else {

			//If user is not Admin
			return false
		}

	})
}