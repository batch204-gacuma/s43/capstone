const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../Auth.js");


//Controller for User Registration
module.exports.registerUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {

		if(result !== null) {
			return false
		} else {


		let newUser = new User({
				email: reqBody.email,
				password: bcrypt.hashSync(reqBody.password, 10),
				isAdmin: reqBody.isAdmin

		});

		return newUser.save().then((user, error) => {

			if(error) {
				return false
			} else {
				return true
			}

		})
	}
	})
}


module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {

		if(result == null) {
			
			return false

		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect) {

				console.log(result)

				return { access: auth.createAccessToken(result)}

				
			} else {

				return false
			}
		}

	})
}

