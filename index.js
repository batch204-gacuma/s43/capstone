const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");

const port = process.env.PORT || 4000;

const app = express();

mongoose.connect("mongodb+srv://edvicgacuma:admin123@batch204gacuma.zhxhthj.mongodb.net/s42-s45?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});



let db = mongoose.connection;
db.on("error", () => console.error.bind(console, "Error"));
db.once("open", () => console.log("connected to MongoDB"))

app.use(cors());
app.use(express.json());

app.use("/users", userRoutes);
app.use("/product", productRoutes)




app.listen(port, ()=> {
	console.log(`listening to ${port}...`)
});