const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orders: [
		{
			product: [
				{
					productName: {
						type: String,
						default: ""
					},
					quantity: {
						type: Number,
						default: ""
					}
				}
			],
			totalAmount: {
				type: Number,
				default: ""
			},
			purchasedOn: {
				type: Date,
				default : new Date()
			}
		}
	]



})










module.exports = mongoose.model("User", userSchema);